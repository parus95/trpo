import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratorSchemeComponent } from './generator-scheme.component';

describe('GeneratorSchemeComponent', () => {
  let component: GeneratorSchemeComponent;
  let fixture: ComponentFixture<GeneratorSchemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeneratorSchemeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorSchemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('bitsequence reversed', () => {
    component.setPolyBin('1010');
    expect(component.triggers).toEqual([false, true, false, true],
       'Начальный коэффициент относится к старшей степени. Соответствующий триггер расположен самым правым.'
    );
  });
});
