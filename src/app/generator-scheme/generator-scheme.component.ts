import { Component, OnInit } from '@angular/core';
import { Convert } from '../convert';

@Component({
  selector: 'app-generator-scheme',
  templateUrl: './generator-scheme.component.html',
  styleUrls: ['./generator-scheme.component.scss']
})
export class GeneratorSchemeComponent implements OnInit {
  public triggers: boolean[] = [];

  constructor() { }

  ngOnInit() {
  }


  public setPolyBin(polyBin: string) {
    this.triggers = Convert.strToBools(polyBin).reverse();
  }
}
