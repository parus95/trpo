import { Convert } from './convert';

export class McodeGenerator {
    coeffs: (1 | 0)[];
    constructor(polyBin: string) {
        this.coeffs = Convert.strToBinaryCoeffs(polyBin);
    }

    public convertPhase(phaseBin: string) {
        const phaseUnpadded = Convert.strToBinaryCoeffs(phaseBin);
        return Array(this.coeffs.length - phaseUnpadded.length).fill(0).concat(phaseUnpadded);
    }
    public generate(phaseBin: string): (1 | 0)[] {
        const initialPhase = this.convertPhase(phaseBin);
        
        if (initialPhase.every((value) => value === 0)) {
            throw new Error('Intial phase cant be zero');
        }

        const ph = initialPhase.map(x => x);

        const result: (1 | 0)[] = [];
        do {
            const n = this.next(ph);
            result.push(ph.shift()); // Берём выталкиваемое значение из правого триггера
            ph.push(n);
        } while (!ph.every((value, index) => value === initialPhase[index]));
        return result;
    }
    next(a: (1 | 0)[]): (1 | 0) {
        const temp = this.coeffs.map((value, index) => a[index] * value);
        return (temp.reduce((prev, curr) => prev + curr, 0) % 2) === 0 ? 0 : 1;
    }
}
