import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Input } from '@angular/core';
import * as c3 from 'c3';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, AfterViewInit {

  @ViewChild('chart', { read: ElementRef }) chartEl: ElementRef;
  @Input()
  public title = 'test';

  chart: c3.ChartAPI;

  constructor() {
  }

  ngOnInit() {
  }


  ngAfterViewInit() {
    this.chart = c3.generate({
      bindto: this.chartEl.nativeElement as HTMLElement,
      data: {
        columns: [
        ]
      }
    });
  }


  public setValues(values: number[]) {
    this.chart.load({
      columns: [
        [this.title, ...values]
      ]
    });
  }

}
