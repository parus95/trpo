import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { PolyTable } from '../poly-table';

@Component({
  selector: 'app-poly-select',
  templateUrl: './poly-select.component.html',
  styleUrls: ['./poly-select.component.css']
})
export class PolySelectComponent implements OnInit {
  public availLengths: number[];
  public availValues: string[];
  public binary: string;

  @Output()
  public polyChange: EventEmitter<string>;

  constructor() {
    this.availLengths = Object.keys(PolyTable).map(x => +x);
    this.polyChange = new EventEmitter<string>();

  }

  ngOnInit() {
  }

  public loadPoly() {
    this.lengthSelected(this.availLengths[0]);
  }

  public lengthSelected(len: number) {
    this.availValues = PolyTable[len];
    this.polySelected(this.availValues[0]);
  }
  public polySelected(poly: string) {
    const polyV = Number.parseInt(poly, 16);
    this.binary = polyV.toString(2);

    this.polyChange.emit(this.binary);
  }
}
