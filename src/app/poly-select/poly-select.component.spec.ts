import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolySelectComponent } from './poly-select.component';

describe('PolySelectComponent', () => {
  let component: PolySelectComponent;
  let fixture: ComponentFixture<PolySelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolySelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolySelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
