import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ChartComponent } from './chart/chart.component';
import { GeneratorSchemeComponent } from './generator-scheme/generator-scheme.component';
import { PolySelectComponent } from './poly-select/poly-select.component';


@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    GeneratorSchemeComponent,
    PolySelectComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
