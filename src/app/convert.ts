export class Convert {


    private static strToBinary<T>(str: string, one: T, zero: T): T[] {
        return str.split('').map(x => {
            switch (x) {
                case '0':
                    return zero;
                case '1':
                    return one;
                default:
                    throw new Error('Incorrect binary digit');
            }
        });
    }
    static strToBools(str: string): boolean[] {
        return this.strToBinary(str, true, false);
    }
    static strToBinaryCoeffs(str: string): (1 | 0)[] {
        return this.strToBinary<1 | 0>(str, 1, 0);
    }
}
