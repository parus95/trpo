// http://users.ece.cmu.edu/~koopman/lfsr/
export const PolyTable: { [size: number]: string[] } = {
    4: ['9', 'C'],
    5: ['12', '14', '17', '1B', '1D', '1E'],
    6: ['21', '2D', '30', '33', '36', '39'],
    7: ['41', '44', '47', '48', '4E', '53', '55', '5C', '5F', '60', '65',
        '69', '6A', '72', '77', '78', '7B', '7E'],
};
