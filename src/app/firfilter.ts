import { directConvolution } from 'ml-convolution';

export class Firfilter {
    constructor(private kernel: number[]) {

    }
    public calculate(signal: number[]): number[] {
        return directConvolution(signal, this.kernel);
    }
}
