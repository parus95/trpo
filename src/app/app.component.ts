import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { GeneratorSchemeComponent } from './generator-scheme/generator-scheme.component';
import { ChartComponent } from './chart/chart.component';
import { PolySelectComponent } from './poly-select/poly-select.component';
import { McodeGenerator } from './mcode-generator';
import { Firfilter } from './firfilter';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'app';

  @ViewChild('scheme', { read: GeneratorSchemeComponent }) scheme: GeneratorSchemeComponent;
  @ViewChild('chartSequence', { read: ChartComponent }) chartSequence: ChartComponent;
  @ViewChild('chartAutoCorrelation1', { read: ChartComponent }) chartAutoCorrelation1: ChartComponent;
  @ViewChild('chartAutoCorrelation3', { read: ChartComponent }) chartAutoCorrelation3: ChartComponent;
  @ViewChild('polySelected', { read: PolySelectComponent }) polySelected: PolySelectComponent;

  public polyChanged(polyBin: string) {
    this.scheme.setPolyBin(polyBin);

    const mSequence = new McodeGenerator(polyBin).generate('1').map(x => x === 0 ? 1 : -1);

    this.chartSequence.setValues(mSequence);

    const filter = new Firfilter(mSequence.map(x => x).reverse());

    this.chartAutoCorrelation1.setValues(filter.calculate(mSequence));

    this.chartAutoCorrelation3.setValues(filter.calculate([...mSequence, ...mSequence, ...mSequence]));

  }

  ngAfterViewInit() {
    this.polySelected.loadPoly();
  }
}
