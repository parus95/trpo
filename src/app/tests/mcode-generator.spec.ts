import { McodeGenerator } from '../mcode-generator';

describe('MCodeGenerator', () => {

    it('Works', () => {
        const generator = new McodeGenerator('110');
        const result = generator.generate('1');
        console.log(result);
        expect(result).toEqual([0, 0, 1, 0, 1, 1, 1]);
    });

    it ('GenerateWithNullPhase', () => {
        const generator = new McodeGenerator('110');
        expect(() => { generator.generate('0'); })
            .toThrowError('Intial phase cant be zero');
    });

    it ('GenerateWithEmptyPhase', () => {
        const generator = new McodeGenerator('110');
        expect(() => { generator.generate(''); })
            .toThrowError('Intial phase cant be zero');
    });

    it ('NonTablePolynom', () => {
        const generator = new McodeGenerator('1001101');
        const result = generator.generate('1');
        expect(result.length).toBeLessThan(127);
        // У табличного полинома длина должна рассчитываться, как 2^m - 1, т.е. при нашем входном длина=2^7-1=128.
        // Если же полином нетабличный то его длина будет в разы меньше. Проверку чего и осуществляет данный тест.
    });
});
