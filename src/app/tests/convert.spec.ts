import { Convert } from '../convert';

describe('Convert', () => {

    it('strToBinaryCoeffs', () => {
        const result = Convert.strToBinaryCoeffs('010100');
        expect(result).toEqual([0, 1, 0, 1, 0, 0]);
    });

    it('strToBinaryCoeffs.withInvalidInput', () => {
        expect(() => { Convert.strToBinaryCoeffs('010102'); })
            .toThrowError('Incorrect binary digit');
    });

    it('strToBools', () => {
        const result = Convert.strToBools('010100');
        expect(result).toEqual([false, true, false, true, false, false]);
    });

    it('strToBools.withInvalidInput', () => {
        expect(() => { Convert.strToBools('010102'); })
            .toThrowError('Incorrect binary digit');
    });

});
