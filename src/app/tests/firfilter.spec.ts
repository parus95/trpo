import { Firfilter } from '../firfilter';

describe('FIRFilter', () => {

    it('KernelSequence', () => {
        const firFilter = new Firfilter([1, 2, 3, 4, 5]);
        const response = firFilter.calculate([1]);
        expect(response).toEqual([1, 2, 3, 4, 5]);
    });
    it('KernelAndInputSequence', () => {
        const firFilter = new Firfilter([1, 2]);
        const response = firFilter.calculate([3, 4]);
        expect(response).toEqual([
            // Текущее значение * 1 + Предыдущее значение * 2
            3 * 1 + 0 * 2,
            4 * 1 + 3 * 2,
            0 * 1 + 4 * 2
        ]);
    });
});
